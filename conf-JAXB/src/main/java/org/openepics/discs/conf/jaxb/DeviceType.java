/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.jaxb;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This is data transfer object representing a CCDB device type for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name = "deviceType")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Artifact.class})
public class DeviceType {
    private String name;
    private String description;

    @XmlElementWrapper(name = "artifacts")
    @XmlAnyElement(lax = true)
    private List<Artifact> artifacts;

    public DeviceType() { }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public List<Artifact> getArtifacts() { return artifacts; }
    public void setArtifacts(List<Artifact> artifacts) { this.artifacts = artifacts; }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("DeviceType[");
        str.append("name=").append(name);
        str.append(", description=").append(description);
        str.append(", artifacts=");
        if (artifacts != null) {
          str.append(Arrays.toString(artifacts.toArray()));
        } else {
            str.append("null");
        }
        str.append("]");
        return str.toString();
    }
}
