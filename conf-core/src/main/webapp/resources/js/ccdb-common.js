/**
 * 
 */

/** Global CCDB environment */
var CCDB = {
        "config" : { 
            "jumpToElementOnLoad" : false,
            "tableHasContent": -1,
            "selectionInProgress" : false
        },
        "jumpToElementOnLoadHandler" : function(xhr, target) {
            selectEntityInTable(CCDB.dataLoaderInternal.dataKey, CCDB.dataLoaderInternal.tableVarName);
        },
        "dataLoaderInternal" : {},
        "oldABHandler" : null,
        "disabledAjaxButton" : null,
        "newABHandler" : function(a, c) {
            if (a.s) {
                var sourceId = PrimeFaces.escapeClientId(a.s);
                var widget = $(sourceId);
                if (widget.hasClass("ui-button") && (CCDB.disabledAjaxButton == null)) {
                    CCDB.disabledAjaxButton = widget;
                    widget.removeClass('ui-state-hover ui-state-focus ui-state-active').addClass('ui-state-disabled')
                        .attr('disabled', 'disabled');
                    jQuery(document).on("pfAjaxComplete", CCDB.ajaxButtonHandler);
                }
            }
            CCDB.oldABHandler.call(PrimeFaces, a, c);
        },
        "takeOverAb" : function() {
            if (!CCDB.oldABHandler) {
                CCDB.oldABHandler = PrimeFaces.ab;
                PrimeFaces.ab = CCDB.newABHandler;
            }
        },
        "ajaxButtonHandler" : function(xhr, target, errorThrown) {
            CCDB.disabledAjaxButton.removeClass('ui-state-disabled').removeAttr('disabled');
            CCDB.disabledAjaxButton = null;
            jQuery(document).off("pfAjaxComplete", CCDB.ajaxButtonHandler);
        },
        "delayedSpinnerTimeout" : null
}

function removeSessionIdAndParametersFromUrl() {
	var modification = false;
	var href = window.location.href;

	if (window.location.search != "") {
		href = href.replace(window.location.search, "");
		modification = true;
    }

	if (href.indexOf(";jsessionid") >= 0) {
		href = href.substring(0, href.indexOf( ";jsessionid" ));
		modification = true;
	}
	
    if (modification) {
        window.history.pushState("", "", href);
    }
}

function emHeight() {
    return $("#top").outerHeight(true) - $("#top").outerHeight();
}

function adjustFooterPosition() {
    var footerWidth = $(".footer-message").outerWidth(true);
    $(".footer-message").css({"left":(window.innerWidth-footerWidth)/2});
}

function startDownload() {
    PF("statusDialog").show();
}

function scrollSelectedIntoView(tableWidget) {
    if (CCDB.config.selectionInProgress) setTimeout(function() {scrollSelectedIntoView(tableWidget);}, 100);

    var tableSelection = getTableSelection(tableWidget);
    if (isRowSelected(tableWidget)) {
        var scrollableBodyHeight = tableWidget.scrollBody[0].clientHeight;
        var selectedNodeSelector = "tr[data-rk='" + tableSelection[0] + "']";
        var selectedNode = tableWidget.jq.find(selectedNodeSelector);
        if (selectedNode.length <= 0) return;
        var selectedNodePosition = selectedNode[0].offsetTop;
        var lowerLimit = Math.max(0, selectedNodePosition - scrollableBodyHeight + selectedNode.outerHeight());
        if (tableWidget.scrollBody.scrollTop() < lowerLimit || tableWidget.scrollBody.scrollTop() > selectedNodePosition) {
            tableWidget.scrollBody.scrollTop(Math.max(0, selectedNodePosition - scrollableBodyHeight / 2));
        }
    } else {
        tableWidget.scrollBody.scrollTop(0);
    }
}

/**
 * selectEntityInTable() only works on page load, so it can be used for navigating to an entity form another screen. 
 * @param dataKey the global index of the entity
 * @param tableVarName the name of the PrimeFaces table variable
 * @param force if defined and true, the method can be used even if the page has not been loaded
 */
function selectEntityInTable(dataKey, tableVarName, force) {
    if ((typeof force == 'boolean') && (force == true)) CCDB.config.jumpToElementOnLoad = true;
    // selectEntityInTable() only works on page load, so it can be used for navigating to
    //     an entity form another screen.
    bootstrapDataLoader(dataKey, tableVarName);
    var tableWidget = PF(tableVarName);
    // check whether the data is here. If not, wait a little...
    if ((CCDB.config.tableHasContent < 3) && (tableWidget.jq.find('tr.ui-widget-content').length <= 0)) {
        CCDB.config.tableHasContent++;
        setTimeout(function() {selectEntityInTable(dataKey, tableVarName, false);}, 300);
    }

    // table data loaded
    CCDB.config.tableHasContent = -1;
    // search for entity
    var rowRef = tableWidget.jq.find("tr[data-rk='" + dataKey + "']");

    if (rowRef.length > 0) {
        // entity found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
        tableWidget.unselectAllRows();
        tableWidget.selectRow(rowRef);
        scrollSelectedIntoView(tableWidget);
    } else if (!tableWidget.allLoadedLiveScroll) {
        // not found, but there's more data
        tableWidget.loadLiveRows();
    } else {
        // entity not found
        CCDB.config.selectionInProgress = false;
        jQuery(document).off("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
    }
}

function bootstrapDataLoader(dataKey, tableVarName) {
    if (CCDB.config.jumpToElementOnLoad == true) {
        CCDB.config.selectionInProgress = true;
        CCDB.config.jumpToElementOnLoad = false;
        // store the parameters
        CCDB.dataLoaderInternal = {
                "dataKey" : dataKey,
                "tableVarName" : tableVarName
        }
        jQuery(document).on("pfAjaxComplete", CCDB.jumpToElementOnLoadHandler);
    }
}

function resizeDeleteList(deleteDialogId) {
    var deleteTable = $("#" + deleteDialogId + " .dialogListTable .ui-datatable-scrollable-body");
    var tableContentHeight = $("#" + deleteDialogId + " .dialogListTable .ui-datatable-data").outerHeight(true);
    var calculatedHeight = emHeight() * 13;
    if (tableContentHeight >= emHeight() * 25) {
        calculatedHeight = emHeight() * 25;
    } else if (tableContentHeight >= emHeight() * 13) {
        calculatedHeight = tableContentHeight + 2;
    }
    deleteTable.css({"height":calculatedHeight});
}

function initializeDataTable(scrollDataCount) {
    var datatable = PF('attributesDataTable');
    datatable.cfg.scrollLimit = scrollDataCount;
}

function dataTableLiveScrollFix() {
    var datatable = PF('attributesDataTable');
    // this is for PrimeFaces bug
    var actuallyLoaded = $(datatable.jqId).find(".ui-datatable-scrollable-body").
                                        find(".ui-datatable-data").find(".ui-widget-content").length;
    datatable.scrollOffset = actuallyLoaded;
    datatable.allLoadedLiveScroll = actuallyLoaded >= datatable.cfg.scrollLimit;
    datatable.shouldLiveScroll = !datatable.allLoadedLiveScroll;
}

function dataTableAjaxFix(xhr, status, args) {
    if (status && status.responseXML) {
        if (status.responseXML.getElementById('deviceTypesForm:attributesDataTable:attributesDataTable') || 
                status.responseXML.getElementById('devicesForm:attributesDataTable:attributesDataTable') || 
                status.responseXML.getElementById('hierarchies:content:attributesDataTable:attributesDataTable')) {
            dataTableLiveScrollFix();
        }
    } 
}

function delayedSpinnerStart() {
    if (CCDB.delayedSpinnerTimeout == null) {
        CCDB.delayedSpinnerTimeout = setTimeout(function(){PF('statusDialog').show();} , 1100);
    }
}

function delayedSpinnerStop() {
    clearTimeout(CCDB.delayedSpinnerTimeout);
    CCDB.delayedSpinnerTimeout = null;
    PF('statusDialog').hide();
}

/**
 * Resets the table state for live scrolling. Setting all values the same as on the initial page load.
 * @param tableWidget
 */
function resetLiveTableState(tableWidget) {
    tableWidget.scrollOffset = 0;
    tableWidget.allLoadedLiveScroll = (tableWidget.cfg.scrollLimit <= tableWidget.scrollOffset - tableWidget.cfg.scrollStep);
    tableWidget.shouldLiveScroll = true;
    setTimeout(function() {scrollSelectedIntoView(tableWidget);}, 300);
}

function getTableSelection(tableWidget) {
    if (tableWidget.selection === undefined) {
        return tableWidget.selections;
    } else {
        return tableWidget.selection;
    }
}

function isRowSelected(tableWidget) {
    return getTableSelection(tableWidget).length > 0;
}

function selectWithDelay(dataKey, tableVarName, delay) {
    setTimeout(function() {selectEntityInTable(dataKey, tableVarName, true);}, delay);
}
