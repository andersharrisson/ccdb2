/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.util.qr;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.openepics.discs.conf.util.AppProperties;
import org.openepics.discs.conf.util.HelperProperties;

/**
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class QRSlotData implements QRData {

    private static final String CCDB_QR_URL;

    static {
        String homeUrl = HelperProperties.getProperty(AppProperties.CONF_HOME_URL_PROPERTY_NAME, "https://ccdb.esss.lu.se/");
        if (!homeUrl.endsWith("/")) {
            homeUrl += "/";
        }
        CCDB_QR_URL = homeUrl + "?name=";
    }

    private final String url;
    private final String title;

    public QRSlotData(final String slotName) {
        String url;
        try {
            url = CCDB_QR_URL + URLEncoder.encode(slotName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            url = CCDB_QR_URL + slotName;
        }
        this.url = url;
        this.title = slotName;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public String getTitle() {
        return title;
    }
}
