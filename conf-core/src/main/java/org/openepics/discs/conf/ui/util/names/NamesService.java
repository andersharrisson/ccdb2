/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.util.names;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateless;

import org.openepics.names.client.NamesClient;
import org.openepics.names.jaxb.DeviceNameElement;

/**
 * This is a service layer that handles accessing naming service through {@link NamesClient}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class NamesService {

    /**
     * Returns a set of all names currently registered in the naming service.
     *
     * @return the set
     */
    public Set<String> getAllNames() {
        final Set<String> names = new HashSet<String>();

        try {
            final NamesClient client = new NamesClient();
            for (DeviceNameElement element : client.getAllDeviceNames()) {
                names.add(element.getName());
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("There was an error retriving data from the naming service.", e);
        }
        return names;
    }

    /**
     * Returns a set of all devices currently registered in the naming service.
     *
     * @return the set
     */
    public Set<DeviceNameElement> getAllDevices() {
        final Set<DeviceNameElement> devices = new HashSet<DeviceNameElement>();
        try {
            final NamesClient client = new NamesClient();
            devices.addAll(client.getAllDeviceNames());
        } catch (RuntimeException e) {
            throw new RuntimeException("There was an error retriving data from the naming service.", e);
        }
        return devices;
    }
}
