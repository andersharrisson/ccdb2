/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.common;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This holds and converts to display format the value to be show in overlay value dialog.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class OverlayValueProvider {

    private static final Logger LOGGER = Logger.getLogger(OverlayValueProvider.class.getCanonicalName());
	
	private String value;
	private String displayValue;
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
        LOGGER.log(Level.FINE, "Setting value: " + value);
		this.value = value;
		displayValue = formatValue(this.value); 
	}
	/**
     * @return A pretty printed representation of the value.
	 */
	public String getDisplayValue() {
        LOGGER.log(Level.FINE, "Getting display value: " + displayValue);
		return displayValue;
	}

    private String formatValue(String value) {
        if (value == null) {
            return "";
        }
        // Determine in a fast way whether value is possibly JSON
        if (value.startsWith("{") || value.startsWith("[")) {
	        try {
	            final ObjectMapper mapper = new ObjectMapper();
	            final Object json = mapper.readValue(value, Object.class);
	            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
	        } catch (IOException e) {
	            LOGGER.log(Level.FINE, e.getMessage(), e);
	            // treat as standard string and continue
	        }
        }
        return value;
    }
}
