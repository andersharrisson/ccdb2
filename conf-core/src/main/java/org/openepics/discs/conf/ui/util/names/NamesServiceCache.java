package org.openepics.discs.conf.ui.util.names;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.openepics.names.jaxb.DeviceNameElement;

/**
 * A session bean holding caching the data retrieved from {@link NamesService}. This class ensures that the possibly
 * slow read operations from underlying services are not performed more than once in a given session.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
@Lock(LockType.READ)
public class NamesServiceCache implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(NamesServiceCache.class.getName());
    // the time to wait before updating name service cache (5 min )
    private static final String UPDATE_INTERVAL = "*/5";

    @Inject
    private transient NamesService namesService;

    private static boolean valid;

    private Map<String, String> namesStatus;
    private Set<DeviceNameElement> devices;
    private ReentrantLock updateLock = new ReentrantLock();

    @PostConstruct
    private void initialise() {
        LOGGER.log(Level.INFO, "Naming service cache is initializing.");
        updateCache();
    }

    /**
     * Updated name service cache.
     * 
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        updateCacheNonBlocking();
    }

    /**
     * Invokes update and if it is already in progress blocks until it is finished
     */
    public void updateCacheBlocking() {
        if (!updateCache()) {
            LOGGER.log(Level.INFO, "Name service cache is already beeing updated. Will hold until it is done.");
            try {
                // We try to acquire lock waiting for 20 seconds. If unsuccessful we try until successful.
                while (!updateLock.tryLock(20, TimeUnit.SECONDS))
                    ;
                updateLock.unlock();
                LOGGER.log(Level.INFO, "Naming service update finished.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Invokes update and if it is already in progress skips
     */
    public void updateCacheNonBlocking() {
        if (!updateCache()) {
            LOGGER.log(Level.INFO, "Name service cache is already beeing updated. Will skip...");
        }
    }

    /**
     * Helper method that invokes cache updating and returns true if updating is not already in progress, else false
     */
    private boolean updateCache() {
        if (updateLock.tryLock()) {
            try {
                LOGGER.log(Level.INFO, "Updating name service cache.");
                Set<DeviceNameElement> newDevices = updateDevices();
                Map<String, String> newNamesStatus = generateNamesStatusMap(newDevices);

                devices = newDevices;
                namesStatus = newNamesStatus;
                LOGGER.log(Level.INFO, "Name service cache has been updated.");
            } finally {
                updateLock.unlock();
            }
            return true;
        }
        return false;
    }

    private Set<DeviceNameElement> updateDevices() {
        Set<DeviceNameElement> newDevices;
        try {
            newDevices = namesService.getAllDevices();
            valid = true;
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "There was an exception retrieving devices from the naming service.", e);
            valid = false;
            newDevices = Collections.emptySet();
        }
        return newDevices;
    }

    /**
     * Returns a set of all devices currently registered in the naming service.
     *
     * @return the set
     */
    public Set<DeviceNameElement> getAllDevices() {
        if (devices != null && (!devices.isEmpty())) {
            return devices;
        }
        updateCache();
        return devices;
    }

    private Map<String, String> generateNamesStatusMap(Set<DeviceNameElement> newDevices) {
        namesStatus = new HashMap<String, String>();
        for (DeviceNameElement element : newDevices) {
            namesStatus.put(element.getName(), element.getStatus());
        }
        return namesStatus;
    }

    /**
     * Returns a set of all names currently registered in the naming service.
     *
     * @return the set
     */
    public Set<String> getAllNames() {
        if (namesStatus == null) {
            updateCache();
        }
        return namesStatus.keySet();
    }

    /**
     * Returns a list of all names currently registered in the naming service.
     *
     * @return the list
     */
    public List<String> getAllNamesList() {
        if (namesStatus == null) {
            updateCache();
        }
        List<String> results = new ArrayList<String>(getAllNames());
        Collections.sort(results);
        return results;
    }

    /**
     * @return true if the cache contains valid data, false if the data retrieval from the names service failed
     */
    public boolean isValid() {
        return valid;
    }
}
