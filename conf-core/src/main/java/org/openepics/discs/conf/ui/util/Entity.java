/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.ui.util;

/**
 * Entity type strings
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public enum Entity {
    TAG("tag"),
    UNIT("unit"),
    UNITS("units"),
    SLOT("slot"),
    SLOTS("slots"),
    ARTIFACT("artifact"),
    DEVICE("device"),
    DEVICES("devices"),
    PROPERTY("property"),
    PROPERTIES("properties"),
    RELATIONSHIP("relationship"),
    RELATIONSHIPS("relationships"),
    ENUMERATION("enumeration"),
    ENUMERATIONS("enumerations"),
    DEVICE_TYPE("device type"),
    DEVICE_TYPES("device types"),
    DEVICE_PROPERTIES("device properties"),
    DEVICE_TYPE_PROPERTIES("device type properties"),
    SLOT_PROPERTIES("slot properties"),
    QR_PDF("QR document");

    private final String entity;

    private Entity(String entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        return entity;
    }

    public String getCapitalizedString() {
        return entity.substring(0, 1).toUpperCase() + entity.substring(1);
    }
}
