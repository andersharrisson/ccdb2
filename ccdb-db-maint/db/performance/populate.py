#!/usr/bin/python
#
# Requirements to run this script : python and pg8000
# To install pg8000 : pip install pg8000

import pg8000 as pg
from datetime import datetime
from collections import namedtuple
import math
import random
import string
from config import *

connection = pg.connect(user = databaseUser, password = databasePassword, host = databaseHost, port = databasePort, database = databaseName)
cursor = connection.cursor()

populateInParts = 100

minSlotCount = int(math.ceil(float(slotPropertiesCount) / slotPropertiesPerSlot))
if slotCount < minSlotCount: 
    slotCount = minSlotCount

user = "user"
deviceType = "devicetype"
container = "container"
property = "property"
device = "device"

cursor.execute("SELECT id FROM slot WHERE name = '_ROOT'")
rootSlot = str(cursor.fetchone()[0])

cursor.execute("SELECT id FROM slot_relation WHERE name = 'CONTAINS'")
containsRelation = str(cursor.fetchone()[0])

cursor.execute("SELECT MAX(slot_order) FROM slot_pair WHERE parent_slot = '" + rootSlot + "'")
result = cursor.fetchone()[0]
lastRootSlotOrder = (result if result else 0) + 1

cursor.execute("SELECT id FROM data_type WHERE name = 'String'")
stringTypeId = str(cursor.fetchone()[0])

def getName(name, i):
    return name + str(i) + "_"

def namingConventionName(i):
    return "Sec-Sub:Dis-Dev-" + str(i) + "_"

lastId = 2000000

def getId():
	global lastId
	id = lastId
	lastId += 1
	return str(id)

def getFromList(aList, index):
	return str(aList[index % len(aList)])

def getRootSlotOrder():
	global lastRootSlotOrder
	rootSlotOrder = lastRootSlotOrder
	lastRootSlotOrder += 1
	return str(rootSlotOrder)

###################
### DEVICE TYPE ###
###################
print('Creating ' + str(deviceTypeCount) + ' device types')

# id, modified_at,               modified_by, version, description, name, super_component_type
# 45	 2016-07-13 15:03:58.929	system	     0        _ROOT	    _ROOT \N	               
deviceTypeColumn = ["id", "modified_at", "modified_by", "version", "description", "name"]
deviceTypeValues = []
for i in range(deviceTypeCount):
	id = getId()
	modified_at = str(datetime.now())
	modified_by = user
	version = str(0)
	description = getName(deviceType, i)
	name = getName(deviceType, i)
	deviceTypeValues.append((id, modified_at, modified_by, version, description, name))

cursor.execute("INSERT INTO component_type " + "(" + ", ".join(deviceTypeColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in deviceTypeValues) + " RETURNING id")
deviceTypeIds = [id[0] for id in cursor.fetchall()]

#####################
### PROPERTY TYPE ###
#####################
print('Creating ' + str(propertyTypeCount) + ' property types')

# id, modified_at,               modified_by, version, description, name,       value_unique, data_type, unit
# 50  2017-03-28 11:31:13.977    admin        0        property_    property_    NONE         36         \N
propertyColumn = ["id", "modified_at", "modified_by", "version", "description", "name", "value_unique", "data_type"]
propertyValues = []
for i in range(propertyTypeCount):
    id = getId()
    modified_at = str(datetime.now())
    modified_by = user
    version = str(0)
    description = getName(property, i)
    name = getName(property, i)
    value_unique = 'NONE'
    data_type = stringTypeId
    propertyValues.append((id, modified_at, modified_by, version, description, name, value_unique, data_type))

cursor.execute("INSERT INTO property " + "(" + ", ".join(propertyColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in propertyValues) + " RETURNING id")
propertyIds = [id[0] for id in cursor.fetchall()]


##################################
### CONTAINER, SLOT, SLOT PAIR ###
##################################
print('Creating ' + str(slotCount) + ' slots')

Slot = namedtuple('Slot', 'id hosting')
SlotPair = namedtuple('SlotPair', 'child parent')

slots = []
slotPairs = []

for i in range(slotCount):
	slots.append(Slot(getId(), True))
	
parentlessSlotIds = [slot.id for slot in slots]
parentSlotId = None
childCount = 0
while len(parentlessSlotIds) > 1:
	slotId = parentlessSlotIds.pop(0)

	if parentSlotId is None or slotId == parentSlotId or childCount > 3:
		parentSlotId = getId()
		slots.append(Slot(parentSlotId, False))
		parentlessSlotIds.append(parentSlotId)
		childCount = 0
		
	slotPairs.append(SlotPair(slotId, parentSlotId))
	childCount += 1

slotId = parentlessSlotIds.pop(0)
slotPairs.append(SlotPair(slotId, rootSlot))
	
# id, modified_at,              modified_by, version, asm_comment, asm_position, comment, description, is_hosting_slot, name,                       asm_slot, component_type
# 56  2017-03-17 15:25:50.384	admin	     0	      \N	       \N	         \N	      Slot1	       t	            FEB-050ROW:VAC-VPN-20000	\N	      50

slotColumn = ["id", "modified_at", "modified_by", "version", "description", "is_hosting_slot", "name", "component_type"]
slotValues = []
i = 0
for slot in slots:
    id = slot.id
    modified_at = str(datetime.now())
    modified_by = user
    version = str(0)
    name = namingConventionName(i) if slot.hosting else getName(container, i)
    description = name
    is_hosting_slot = str(slot.hosting)
    component_type = getFromList(deviceTypeIds, i)
    i += 1
    slotValues.append((id, modified_at, modified_by, version, description, is_hosting_slot, name, component_type))

cursor.execute("INSERT INTO slot " + "(" + ", ".join(slotColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in slotValues) + " RETURNING id")
slotIds = [id[0] for id in cursor.fetchall()]

# id, slot_order, version, child_slot, parent_slot, slot_relation
# 53  1	          0        52	       47	        42
# 57  1        	  0	       56	       52	        42

slotPairColumn = ["id", "slot_order", "version", "child_slot", "parent_slot", "slot_relation"]
slotPairValues = []
i = 0
for slotPair in slotPairs:
    id = getId()
    slot_order = getRootSlotOrder()
    version = str(0)
    child_slot = str(slotPair.child)
    parent_slot = str(slotPair.parent)
    slot_relation = containsRelation
    slotPairValues.append((id, slot_order, version, child_slot, parent_slot, slot_relation))
    i += 1

cursor.execute("INSERT INTO slot_pair " + "(" + ", ".join(slotPairColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in slotPairValues) + " RETURNING id")
slotPairIds = [id[0] for id in cursor.fetchall()]
connection.commit()

#######################
### SLOT PROPERTIES ###
#######################
#  id,  modified_at,              modified_by, version, in_repository, prop_value, property,  unit, slot
#  52   2017-03-28 11:41:57.68    admin        0        f              <struct>    2000001    \N    2000014
#  prop_value
#  {"meta":{"type":"SedsScalar_String","protocol":"SEDSv1","version":"1.0.0"},"data":{"value":"Value","representation":"Value"}}

propertyValueBase = ''.join(random.choice(string.lowercase) for x in range(propertyValueSize))

slotPropertyColumn = ["id", "modified_at", "modified_by", "version", "in_repository", "prop_value", "property", "slot"]
slotPropertyValues = []


for j in range(populateInParts):
    print(str(j + 1) + '. part of population')
    
    print('Creating ' + str(slotPropertiesCount/populateInParts) + ' properties')
    slotPropertyValues = []
    for i in range(slotPropertiesCount * j/populateInParts, slotPropertiesCount * (j + 1) / populateInParts):
        id = getId()
        modified_at = str(datetime.now())
        modified_by = user
        version = str(0)
        in_repository = 'f'
        value = getName(propertyValueBase, i)
        prop_value = '{"meta":{"type":"SedsScalar_String","protocol":"SEDSv1","version":"1.0.0"},"data":{"value":"' + value + '","representation":"' + value + '"}}'
        property = getFromList(propertyIds, i)
        slot = getFromList(slotIds, i)
        slotPropertyValues.append((id, modified_at, modified_by, version, in_repository, prop_value, property, slot))
    
    cursor.execute("INSERT INTO slot_property_value " + "(" + ", ".join(slotPropertyColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in slotPropertyValues))
    connection.commit()

print('Done')
print('Created a total of:')
print('    ' + str(slotPropertiesCount) +  ' properties')




# generates random text of length
def getDescription(length):
    return ''.join(random.choice(string.lowercase) for x in range(length))

# array of operations
operations = ["CREATE", "UPDATE"]
# returns random operation name from array

def getRandomOperation():
    return operations[random.randint(0, len(operations) - 1)]

# array of entity types
entityTypes = ["SLOT", "DATA_TYPE", "PROPERTY", "UNIT", "DEVICE", "COMPONENT_TYPE"]

# return random entity type from array
def getRandomEntityType():
    return entityTypes[random.randint(0, len(entityTypes) - 1)]

############
### LOGS ###
############
# id     entity_id   entity_key  entity_type    entry                                                       log_time                    oper    ccdb_user   version
# 155    47          _ROOT       SLOT           {"name":"_ROOT","description":"Implicit CCDB type.", ...}   2017-04-05 12:43:10.735     UPDATE  admin       0
# name of columns in database
logsColumn = ["id", "entity_id", "entity_key", "entity_type", "entry", "log_time", "oper", "ccdb_user", "version"]
logsValues = []

# template for entity names
namePrefix = "entity"

print('Populating log entries.')
for j in range(populateInParts):
    logsValues = []
    print(str(j + 1) + '. part of population of logs.')
    for i in range(logsCount * j/populateInParts, logsCount * (j + 1) / populateInParts):
        id = getId()
        entityName = namePrefix + id
        entityType = getRandomEntityType()
        entryText = getDescription(textLength)
        logTime = str(datetime.now())
        oper = getRandomOperation()
        version = str(0)
        logsValues.append((id, id, entityName, entityType, entryText, logTime, oper, user, version))

    cursor.execute("INSERT INTO audit_record " + "(" + ", ".join(logsColumn) + ") " + "VALUES " + ", ".join(
        "('" + "', '".join(value) + "')" for value in logsValues) + " RETURNING id")
    connection.commit()
    cursor.close();
    cursor = connection.cursor();

print('Creating ' + str(logsCount) + ' log entries.')

print('Done')

###############
### DEVICE  ###
###############

deviceColumn = ["id", "modified_at", "modified_by", "version", "serial_number", "component_type"]

i = 0
print('Creating devices')

for j in range(populateInParts):
    print(str(j + 1) + '. part of population of devices.')

    deviceValues = []
    for i in range(deviceCount * j / populateInParts, deviceCount * (j + 1) / populateInParts):
        id = getId()
        modified_at = str(datetime.now())
        modified_by = user
        version = str(0)
        name = getName(device, i)
        component_type = getFromList(deviceTypeIds, i)
        i += 1
        deviceValues.append((id, modified_at, modified_by, version, name, component_type))
    cursor.execute("INSERT INTO device " + "(" + ", ".join(deviceColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in deviceValues) + " RETURNING id")
    connection.commit()
cursor.execute("SELECT id FROM device");
deviceIds =[id[0] for id in cursor.fetchall()]

print('Created ' + str(deviceCount) + ' devices.')

#########################
### DEVICE PROPERTIES ###
#########################

propertyValueBase = getDescription(propertyValueSize)

devicePropertyColumn = ["id", "modified_at", "modified_by", "version", "in_repository", "prop_value", "property", "device"]

deviceWithPropertiesCount = len(deviceIds)
x = 0

print('Creating device properties')
for j in range(populateInParts):
    print(str(j + 1) + '. part of population')

    devicePropertyValues = []
    for i in range(deviceWithPropertiesCount * j / populateInParts, deviceWithPropertiesCount * (j + 1) / populateInParts):
        device = getFromList(deviceIds, x)
        modified_at = str(datetime.now())
        modified_by = user
        version = str(0)
        in_repository = 'f'
        for k in range(devicePropertiesPerDevice):
            id = getId()
            value = getDescription(propertyValueSize)
            prop_value = '{"meta":{"type":"SedsScalar_String","protocol":"SEDSv1","version":"1.0.0"},"data":{"value":"' + value + '","representation":"' + value + '"}}'
            property = getFromList(propertyIds, k)
            devicePropertyValues.append((id, modified_at, modified_by, version, in_repository, prop_value, property, device))
        x += 1
    cursor.execute("INSERT INTO device_property_value " + "(" + ", ".join(devicePropertyColumn) + ") " + "VALUES " + ", ".join("('" + "', '".join(value) + "')" for value in devicePropertyValues))
    connection.commit()

print('Done')
print('Created a total of:')
print('    ' + str(deviceWithPropertiesCount * devicePropertiesPerDevice) + ' properties')



cursor.close()
connection.close()

