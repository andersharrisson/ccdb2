/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.webservice;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openepics.discs.conf.ent.ComponentType;
import org.openepics.discs.conf.ent.ComptypePropertyValue;
import org.openepics.discs.conf.ent.DevicePropertyValue;
import org.openepics.discs.conf.ent.EntityWithArtifacts;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.ent.SlotRelationName;
import org.openepics.discs.conf.ent.values.EnumValue;
import org.openepics.discs.conf.ent.values.StrVectorValue;
import org.openepics.discs.conf.jaxb.Artifact;
import org.openepics.discs.conf.jaxb.PropertyKind;
import org.openepics.discs.conf.jaxb.PropertyValue;
import org.openepics.discs.conf.util.BuiltInDataType;
import org.openepics.discs.conf.util.Conversion;
import org.openepics.discs.conf.util.UnhandledCaseException;

public class Utils {
	
    @FunctionalInterface
    interface RelatedSlotExtractor {
        public Slot getRelatedSlot(final SlotPair pair);
    }

    public static List<Artifact> getArtifacts(final EntityWithArtifacts entity) {
        final PropertyKind kind = getKind(entity);
        return entity.getEntityArtifactList().stream().
                map(ea -> {Artifact a = new Artifact(ea); a.setKind(kind); return a; }).collect(Collectors.toList());
    }

    private static PropertyKind getKind(final EntityWithArtifacts entity) {
        if (entity instanceof ComponentType) {
            return PropertyKind.TYPE;
        }
        if (entity instanceof Slot) {
            final Slot slot = (Slot) entity;
            return slot.isHostingSlot() ? PropertyKind.SLOT : PropertyKind.CONTAINER;
        }

        return PropertyKind.DEVICE;
    }

    public static <T> List<T> emptyToNull(List<T> list) {
        return list == null ? null :(list.isEmpty() ? null : list);
    }

    static PropertyValue createPropertyValue(
            final org.openepics.discs.conf.ent.PropertyValue entityPropertyValue) {
        final PropertyValue propertyValue = new PropertyValue();
        final Property parentProperty = entityPropertyValue.getProperty();
        propertyValue.setName(parentProperty.getName());
        propertyValue.setDataType(parentProperty.getDataType() != null ? parentProperty.getDataType().getName() : null);
        propertyValue.setUnit(parentProperty.getUnit() != null ? parentProperty.getUnit().getName() : null);
        propertyValue.setValue(propertyToString(entityPropertyValue));

        if (entityPropertyValue instanceof ComptypePropertyValue) {
            propertyValue.setKind(PropertyKind.TYPE);
        } else if (entityPropertyValue instanceof SlotPropertyValue) {
            propertyValue.setKind(PropertyKind.SLOT);
        } else if (entityPropertyValue instanceof DevicePropertyValue) {
            propertyValue.setKind(PropertyKind.DEVICE);
        } else {
            throw new UnhandledCaseException();
        }

        return propertyValue;
    }

    static private String propertyToString(final org.openepics.discs.conf.ent.PropertyValue entityPropertyValue) {
        final String typeName = entityPropertyValue.getProperty().getDataType().getName();
        switch (typeName) {
            case BuiltInDataType.BOOLEAN_NAME:
                final EnumValue pvEnum = (EnumValue) entityPropertyValue.getPropValue();
                if (pvEnum == null) {
                    return "null";
                }
                return Boolean.toString(pvEnum.toString().equals(Conversion.BOOLEAN_TEXT_TRUE));
            case BuiltInDataType.BOOLEAN_VECTOR_NAME:
                final StrVectorValue pv = (StrVectorValue) entityPropertyValue.getPropValue();
                if (pv == null) {
                    return "null";
                }
                final List<Boolean> bools = pv.getStrVectorValue().stream().
                    map(b -> Boolean.valueOf(b.equals(Conversion.BOOLEAN_TEXT_TRUE))).collect(Collectors.toList());
                return Objects.toString(Arrays.toString(bools.toArray(new Boolean[] {})));
            default:
                return Objects.toString(entityPropertyValue.getPropValue());
        }
    }
    
    static List<String> getRelatedSlots(final Stream<SlotPair> relatedSlotPairs,
            final SlotRelationName relationName,
            final RelatedSlotExtractor extractor) {
        return emptyToNull(relatedSlotPairs.
                sorted((p1, p2) -> Long.compare(p1.getId(), p2.getId())).
                filter(slotPair -> relationName.equals(slotPair.getSlotRelation().getName())).
                map(relatedSlotPair -> extractor.getRelatedSlot(relatedSlotPair)).
                map(slot -> slot.getName()).
                collect(Collectors.toList()));
    }

    static List<String> getSlotNames(final List<Slot> slots) {
    	if (slots == null) {
    		return null;
    	}
        return Utils.emptyToNull(slots.stream().map(s -> s.getName()).collect(Collectors.toList()));
    }
}
