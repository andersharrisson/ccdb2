/*
 * Copyright (c) 2016 European Spallation Source
 * Copyright (c) 2016 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class to help handling the System and file properties.
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
public class HelperProperties {
    private static final Logger LOGGER = Logger.getLogger(HelperProperties.class.getCanonicalName());

    private static Properties fileProperties = new Properties();
    static {
        try (InputStream propFile = AppProperties.class.getClassLoader().getResourceAsStream(AppProperties.CONF_PROPERTIES_FILENAME)) {
            if (propFile != null) {
                fileProperties.load(propFile);
                LOGGER.log(Level.FINER, "Loaded properties from a file: " + fileProperties.stringPropertyNames().toString());
            } else {
                LOGGER.log(Level.FINEST, "Properties file not found.");
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not load file provided properties.", e);
        }
    }

    private HelperProperties() {
        // only static methods.
    }

    /**
     * Get a property as a {@link String}. What you get is a system property (Application Server) if defined, then it
     * tries to search for the values defined inside a {@link AppProperties#CONF_PROPERTIES_FILENAME}. If all fails
     * it uses the default.
     *
     * @param name a {@link String} key for the property
     * @param def the default value
     * @return the property value
     */
    public static String getProperty(String name, String def) {
        if (name == null || name.isEmpty()) {
            return def;
        }
        return System.getProperty(name, fileProperties.getProperty(name, def));
    }

    /**
     * Get a property as an integer. What you get is a system property (Application Server) if defined, then it
     * tries to search for the values defined inside a {@link AppProperties#CONF_PROPERTIES_FILENAME}. If all fails
     * it uses the default.
     *
     * @param name a {@link String} key for the property
     * @param def the default value
     * @return the property value
     */
    public static int getProperty(String name, int def) {
        final String propStrValue = getProperty(name, Integer.toString(def));
        try {
            return Integer.valueOf(propStrValue);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    /**
     * Get a property as a boolean. What you get is a system property (Application Server) if defined, then it
     * tries to search for the values defined inside a {@link AppProperties#CONF_PROPERTIES_FILENAME}. If all fails
     * it uses the default.
     *
     * @param name a {@link String} key for the property
     * @param def the default value
     * @return the property value
     */
    public static final boolean getProperty(String name, boolean def) {
        return "TRUE".equalsIgnoreCase(getProperty(name, Boolean.toString(def)));
    }
}
