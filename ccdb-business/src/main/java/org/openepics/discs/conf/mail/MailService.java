/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 * Controls Configuration Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.openepics.discs.conf.util.AppProperties;

import com.google.common.base.Joiner;

/**
 * This is a service to send mails through configuration specified in JBOSS.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
public class MailService {
    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    @Inject
    private UserDirectoryServiceCache userDirectoryServiceCache;
    @Inject
    private AppProperties properties;

    @Resource(name = "java:jboss/mail/Remote")
    private Session mailSession;

    /**
     * Sends a mail with the specified content.
     *
     * @param recipientNames
     *            the set of user names to set the mail to; the names should be in the same format as for authentication
     *            and authorization
     * @param subject
     *            the subject of the mail
     * @param content
     *            the mail content
     * @param attachments
     *            attachment
     * @param filenames
     *            file name
     * @param withAttachment
     *            if true attachment is added to the message
     * @param footer
     *            whether to include footer with the URL to the application
     */
    public void sendMail(Iterable<String> recipientNames, String loggedInName, String subject, String content,
            List<InputStream> attachments, List<String> filenames, boolean withAttachment, boolean footer) {

        final StringBuilder text = new StringBuilder(content);

        final String homeUrl = properties.getProperty(AppProperties.CONF_HOME_URL_PROPERTY_NAME);

        if (footer && homeUrl != null) {
            text.append("\n");
            text.append("---\n");
            text.append("Controls Configuration Database URL: ");
            text.append(homeUrl);
            text.append("\n");
        }

        try {
            final Message messsage = new MimeMessage(mailSession);

            final List<Address> toAddresses = new ArrayList<>();
            final List<Address> ccAddresses = new ArrayList<>();
            for (final String recipientName : recipientNames) {
                try {
                    final String email = userDirectoryServiceCache.getEmail(recipientName);

                    if (email == null || email.trim().length() == 0) {
                        LOGGER.warning("User '" + recipientName + "' has no email registered in LDAP. Skipping.");
                        continue;
                    }

                    toAddresses.add(new InternetAddress(userDirectoryServiceCache.getEmail(recipientName)));
                } catch (Throwable t) {
                    LOGGER.severe("Could not obtain valid email for user '" + recipientName + "' from LDAP. Skipping.");
                }
            }
            messsage.setRecipients(Message.RecipientType.TO, toAddresses.toArray(new InternetAddress[0]));

            if (toAddresses.isEmpty()) {
                return;
            }

            messsage.setSubject("[CCDB] " + subject);
            messsage.setFrom(new InternetAddress("jboss@esss.lu.se", "Controls Configuration Database"));

            if (loggedInName != null) {
                String replyToEmail = userDirectoryServiceCache.getEmail(loggedInName);
                if (replyToEmail != null && replyToEmail.trim().length() > 0) {
                    InternetAddress userAddress = new InternetAddress(replyToEmail, loggedInName);
                    ccAddresses.add(userAddress);
                    messsage.setReplyTo(new InternetAddress[] { userAddress });
                    messsage.setRecipients(Message.RecipientType.CC, ccAddresses.toArray(new InternetAddress[0]));
                }
            }

            messsage.setSentDate(new Date());

            if (withAttachment && attachments != null && !attachments.isEmpty() && filenames != null
                    && !filenames.isEmpty() && attachments.size() == filenames.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(text.toString(), "text/plain");
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), "application/octet-stream");
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(filenames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                messsage.setContent(multipart);
            } else {
                messsage.setText(text.toString());
            }

            LOGGER.fine("Assembled a message with subject '" + subject + "' to:" + Joiner.on(", ").join(toAddresses)
                    + " cc:" + Joiner.on(", ").join(ccAddresses));
            LOGGER.finest("Message text:\n" + text.toString());

            if (properties.getBooleanPropety(AppProperties.MAIL_NOTIFICATIONS_ENABLED)) {
                Transport.send(messsage);
            }
        } catch (MessagingException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
