/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 * Controls Configuration Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.mail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import se.esss.ics.rbac.access.RBACConnector;
import se.esss.ics.rbac.access.RBACConnectorException;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 * This is a service that provides directory data on users.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
@Startup
public class UserDirectoryService {

    private static final Logger LOGGER = Logger.getLogger(UserDirectoryService.class.getName());

    private static final String TEST_USERNAME = "nonexisting_user";
    private static boolean validated;

    @EJB
    private DirectoryServiceAccess directoryServiceAccess;

    /** Tests that the directory service is reachable, and throws an exception if it isn't. */
    public synchronized void validate() {

        if (validated) {
            return;
        }

        LOGGER.log(Level.INFO, "Checking for presence of user directory.");

        try {
            // test service by trying to retrieve some data
            directoryServiceAccess.getUserInfo(TEST_USERNAME.toCharArray());
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException(e);
        }
        validated = true;
    }

    /**
     * Returns a set of all user names currently registered in the directory.
     * 
     * @return the set
     */
    public Set<String> getAllUsernames() {
        final Set<String> usernames = new HashSet<String>();

        try {
            for (UserInfo userInfo : directoryServiceAccess.getUsers("*", true, false, false)) {
                usernames.add(userInfo.getUsername());
            }
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving data from the user directory.", e);
        }
        return usernames;
    }

    /**
     * Returns a set of names of all users that have administer permission of the cable database.
     * 
     * @return the set
     */
    public Set<String> getAllAdministratorUsernames() {
        Set<String> usernames = new HashSet<String>();
        final String[] permissions = { "WriteAlignmentRecords", "WriteComponentTypes", "WriteDataTypes", "WriteDevices",
                "WriteInstallationRecords", "WriteProperties", "WriteSlots", "WriteUnits" };
        try {
            usernames = new HashSet<String>(Arrays
                    .asList(RBACConnector.getInstance().getUsersWithPermission("ControlsDatabase", permissions[0])));
            for (int i = 1; i < permissions.length; i++) {
                usernames.retainAll(new HashSet<String>(Arrays.asList(
                        RBACConnector.getInstance().getUsersWithPermission("ControlsDatabase", permissions[i]))));
            }
        } catch (RBACConnectorException e) {
            throw new RuntimeException("There was an error retriving data from the RBAC connector.", e);
        }
        return usernames;
    }

    /**
     * Retrieves the email address for the given user.
     * 
     * @param username
     *            the user name
     * @return the user email
     */
    public String getEmail(String username) {
        try {
            return directoryServiceAccess.getUserInfo(username.toCharArray()).getEMail();
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving a user email.", e);
        }
    }

    /**
     * Retrieves fur name for the given user.
     * 
     * @param username
     *            the user name
     * @return the full name of the user
     */
    public String getUserFullName(String username) {
        try {
            UserInfo userInfo = directoryServiceAccess.getUserInfo(username.toCharArray());
            String middleName = userInfo.getMiddleName() != null ? userInfo.getMiddleName() + " " : "";
            return userInfo.getFirstName() + " " + middleName + userInfo.getLastName();
        } catch (DirectoryServiceAccessException e) {
            throw new RuntimeException("There was an error retriving a user full name.", e);
        }
    }

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username
     *            the user name
     * @return the user full name and email
     */
    public String getUserFullNameAndEmail(String username) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(getUserFullName(username));

        final String email = getEmail(username);
        if (email != null) {
            sb.append(" - ").append(email);
        }
        return sb.toString();
    }
}
