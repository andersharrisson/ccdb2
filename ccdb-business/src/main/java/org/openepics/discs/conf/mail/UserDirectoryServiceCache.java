/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 * Controls Configuration Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.mail;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 * A session bean holding caching the data retrieved from {@link UserDirectoryService}. This class ensures that the
 * possibly slow read operations from underlying services are not performed more than once in a given session.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Singleton
public class UserDirectoryServiceCache implements Serializable {

    private static final long serialVersionUID = 1540890790002448065L;
    private static final Logger LOGGER = Logger.getLogger(UserDirectoryServiceCache.class.getName());
    // the time to wait before updating user directory service cache (20 min )
    private static final String UPDATE_INTERVAL = "*/20";

    private Set<String> allUsernames;
    private Set<String> allAdministratorUsernames;

    @Inject
    private transient UserDirectoryService userDirectoryService;
    private ReentrantLock updateLock = new ReentrantLock();

    @PostConstruct
    private void initialise() {
        LOGGER.log(Level.INFO, "User directory service cache is initializing.");
        updateCache();
    }

    /**
     * Update user directory service cache.
     * 
     */
    @Schedule(minute = UPDATE_INTERVAL, hour = "*", persistent = false)
    public void update() {
        updateCacheNonBlocking();
    }

    /**
     * Invokes update and if it is already in progress blocks until it is finished
     */
    public void updateCacheBlocking() {
        if (!updateCache()) {
            LOGGER.log(Level.INFO,
                    "User directory service cache is already beeing updated. Will hold until it is done.");
            try {
                // We try to acquire lock waiting for 20 seconds. If unsuccessful we try until successful.
                while (!updateLock.tryLock(20, TimeUnit.SECONDS))
                    ;
                updateLock.unlock();
                LOGGER.log(Level.INFO, "User directory service update finished.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Invokes update and if it is already in progress skips
     */
    public void updateCacheNonBlocking() {
        if (!updateCache()) {
            LOGGER.log(Level.INFO, "User directory service cache is already beeing updated. Will skip...");
        }
    }

    /**
     * Helper method that invokes cache updating and returns true if updating is not already in progress, else false
     */
    private boolean updateCache() {
        if (updateLock.tryLock()) {
            try {
                LOGGER.log(Level.INFO, "Updating user directory service cache.");
                Set<String> newUsernames = userDirectoryService.getAllUsernames();
                Set<String> newAdministratorUsernames = userDirectoryService.getAllAdministratorUsernames();
                allUsernames = newUsernames;
                allAdministratorUsernames = newAdministratorUsernames;
                LOGGER.log(Level.INFO, "User directory service cache has been updated.");
            } finally {
                updateLock.unlock();
            }
            return true;
        }
        return false;
    }

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set
     */
    public Set<String> getAllUsernames() {

        if (allUsernames != null) {
            return allUsernames;
        }

        updateCache();
        return allUsernames;
    }

    /**
     * Returns a set of names of all users that have administer permission of the cable database.
     *
     * @return the set
     */
    public Set<String> getAllAdministratorUsernames() {

        if (allAdministratorUsernames != null) {
            return allAdministratorUsernames;
        }

        updateCache();
        return allAdministratorUsernames;
    }

    /**
     * Retrieves the email address for the given user.
     *
     * @param username
     *            the user name
     * @return the user email
     */
    public String getEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getEmail(username);
    }

    /**
     * Retrieves the full name of the given user.
     *
     * @param username
     *            the user name
     * @return the user full name
     */
    public String getUserFullName(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullName(username);
    }

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username
     *            the user name
     * @return the user full name and email
     */
    public String getUserFullNameAndEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullNameAndEmail(username);
    }
}
