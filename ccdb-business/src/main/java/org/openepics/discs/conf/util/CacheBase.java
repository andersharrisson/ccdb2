package org.openepics.discs.conf.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is base cache functionality that supports handling cache updates from concurrent requests.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public abstract class CacheBase {
    private static final Logger LOGGER = Logger.getLogger(CacheBase.class.getName());

    protected boolean initialized;
    private boolean valid;

    private ReentrantLock updateLock = new ReentrantLock();

    /**
     * Extend this function to provide cache name used for logging.
     * @return the cache name.
     */
    protected abstract String getCacheName();
    /**
     * Extend this function to update the cache.
     * @return true if the cache was successfully updated, else false.
     */
    protected abstract boolean updateCache();
    
    protected void initialise() {
        LOGGER.log(Level.INFO, getCacheName() + " cache is initializing.");
        updateCacheWithLock();
    }

    /**
     * Updates cache and returns when cache is updated. If cache update operation is already in progress when the method
     * is invoked, it returns when that operation completes. 
     */
    public void updateCacheBlocking() {
    	boolean updateCompleted = updateCacheWithLock();
    	if (updateCompleted) {
    		return;
    	}
    	
        LOGGER.log(Level.INFO, getCacheName() + ": Cache is currently beeing updated. Will wait until it is done.");
        // We try to acquire lock waiting for 20 seconds. If unsuccessful we try until successful.
        try {
            while (!updateLock.tryLock(20, TimeUnit.SECONDS))
                ;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        updateLock.unlock();
        LOGGER.log(Level.INFO, getCacheName() + ": Cache updated.");
    }

    /**
     * Updates cache and returns when cache is updated. If cache update operation is already in progress when the method
     * is invoked, it returns immediately. 
     */
    public void updateCacheNonBlocking() {
    	boolean updateCompleted = updateCacheWithLock();
        if (!updateCompleted) {
            LOGGER.log(Level.INFO, getCacheName() + ": Cache is currently beeing updated. Will skip.");
        }
    }

    /**
     * Updates cache and returns when cache is updated. If cache update operation is already in progress when the method
     * is invoked, it returns immediately and returns false.
     * 
     * @return true if the cache was updated, else false
     */
    private boolean updateCacheWithLock() {
        if (updateLock.tryLock()) {
            try {
                LOGGER.log(Level.INFO, getCacheName() + ": Updating cache.");
                valid = updateCache();
                initialized = true;
                LOGGER.log(Level.INFO, getCacheName() + ": Cache has been updated.");
            } finally {
                updateLock.unlock();
            }
            return true;
        } else {
        	return false;
        }
    }

    /** @return true if the cache contains valid data, else false */
    public boolean isValid() {
        return valid;
    }
}
