/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.auditlog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openepics.discs.conf.ent.AuditRecord;
import org.openepics.discs.conf.ent.EntityType;
import org.openepics.discs.conf.ent.EntityTypeOperation;
import org.openepics.discs.conf.ent.InstallationRecord;
import org.openepics.discs.conf.ent.Slot;
import org.openepics.discs.conf.ent.SlotPair;
import org.openepics.discs.conf.ent.SlotPropertyValue;
import org.openepics.discs.conf.util.Conversion;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

/**
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 *
 */
public class SlotEntityLogger implements EntityLogger<Slot> {
    @Override
    public Class<Slot> getType() {
        return Slot.class;
    }

    @Override
    public List<AuditRecord> auditEntries(Object entity, EntityTypeOperation operation) {
        final Slot slot = (Slot) entity;

        /*
         * TODO This is a hack to correctly generate logs when creating new slot through GUI or data loaders.
         * This MUST be removed and DAO layer should be separated to business logic layer and DAO layer where DAO
         * layer ONLY communicates with the database and business logic layer controls the work flow and other
         * business rules related activities.
         */
        final List<AuditRecord> auditRecords = new ArrayList<>();
        auditRecords.addAll(createAuditRecords(slot, operation));
        if (operation == EntityTypeOperation.CREATE && slot.getPairsInWhichThisSlotIsAChildList().size() == 1) {
            // Create an audit record for the parent slot as well.
            // For parent slot adding a new child is an UPDATE operation.
            auditRecords.addAll(createAuditRecords(slot.getPairsInWhichThisSlotIsAChildList().get(0).getParentSlot(),
                                                        EntityTypeOperation.UPDATE));
        }
        return auditRecords;
    }

    private List<AuditRecord> createAuditRecords(Slot slot, EntityTypeOperation operation) {
        final AuditLogUtil logUtil = new AuditLogUtil(slot)
                        .removeTopProperties(Arrays.asList("id", "modifiedAt", "modifiedBy", "version",
                                "componentType"))
                        .addStringProperty("deviceType", slot.getComponentType().getName())
                        .addArrayOfMappedProperties("slotPropertyList", getPropertiesMap(slot))
                        .addArrayOfMappedProperties("slotArtifactList", EntityLoggerUtil.getArtifactMap(slot))
                        .addArrayOfMappedProperties("childrenList", getChildrenMap(slot))
                        .addArrayOfMappedProperties("parentsList", getParentsMap(slot))
                        .addArrayOfMappedProperties("installation", getInstallationDeviceMap(slot))
                        .addArrayOfProperties("tagList", EntityLoggerUtil.getTagNamesFromTagsSet(slot.getTags()));

        return ImmutableList.of(logUtil.auditEntry(operation, EntityType.SLOT, slot.getName(), slot.getId()));
    }

    private Map<String, String> getPropertiesMap(final Slot slot) {
        final Map<String, String> propertiesMap = new TreeMap<>();
        if (slot.getSlotPropertyList() != null) {
            for (SlotPropertyValue propValue : slot.getSlotPropertyList()) {
                final String entryValue = propValue.getPropValue() == null ? null
                            : propValue.getPropValue().auditLogString(EntityLoggerUtil.AUDIT_LOG_ROWS,
                                                                                EntityLoggerUtil.AUDIT_LOG_COLUMNS);
                propertiesMap.put(propValue.getProperty().getName(), entryValue);
            }
        }
        return propertiesMap;
    }

    private Map<String, Collection<String>> getChildrenMap(final Slot slot) {
        final Multimap<String, String> childrenMap = ArrayListMultimap.create();
        if (slot.getPairsInWhichThisSlotIsAParentList() != null) {
            for (SlotPair slotPair : slot.getPairsInWhichThisSlotIsAParentList()) {
                childrenMap.put(slotPair.getChildSlot().getName(), slotPair.getSlotRelation().getName().toString());
            }
        }
        return childrenMap.asMap();
    }

    private Map<String, Collection<String>> getParentsMap(final Slot slot) {
        final Multimap<String, String> parentsMap = ArrayListMultimap.create();
        if (slot.getPairsInWhichThisSlotIsAChildList() != null) {
            for (SlotPair slotPair : slot.getPairsInWhichThisSlotIsAChildList()) {
                parentsMap.put(slotPair.getParentSlot().getName(), slotPair.getSlotRelation().getIname());
            }
        }
        return parentsMap.asMap();
    }

    private Map<String, String> getInstallationDeviceMap(final Slot slot) {
        final Map<String, String> installationDeviceMap = new TreeMap<>();
        InstallationRecord lastInstallationRecord = null;
        for (InstallationRecord installationRecord : slot.getInstallationRecordList()) {
            if (lastInstallationRecord == null
                    || installationRecord.getModifiedAt().after(lastInstallationRecord.getModifiedAt())) {
                lastInstallationRecord = installationRecord;
            }
        }

        if (lastInstallationRecord != null) {
            final SimpleDateFormat timestampFormat = new SimpleDateFormat(Conversion.DATE_TIME_FORMAT);
            final String installationDeviceSerial = lastInstallationRecord.getDevice().getSerialNumber();
            installationDeviceMap.put("name", installationDeviceSerial);
            installationDeviceMap.put("installationDate",
                        timestampFormat.format(lastInstallationRecord.getInstallDate()));
            if (lastInstallationRecord.getUninstallDate() != null) {
                installationDeviceMap.put("uninstallationDate",
                        timestampFormat.format(lastInstallationRecord.getUninstallDate()));
            }
        }
        return installationDeviceMap;
    }
}
