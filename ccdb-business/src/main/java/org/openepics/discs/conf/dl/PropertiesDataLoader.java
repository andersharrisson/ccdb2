/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.discs.conf.dl;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.annotation.Nullable;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.Pair;
import org.openepics.discs.conf.dl.annotations.PropertiesLoader;
import org.openepics.discs.conf.dl.common.AbstractDataLoader;
import org.openepics.discs.conf.dl.common.DataLoader;
import org.openepics.discs.conf.dl.common.DataLoaderResult;
import org.openepics.discs.conf.dl.common.ErrorMessage;
import org.openepics.discs.conf.ejb.DataTypeEJB;
import org.openepics.discs.conf.ejb.PropertyEJB;
import org.openepics.discs.conf.ejb.UnitEJB;
import org.openepics.discs.conf.ent.DataType;
import org.openepics.discs.conf.ent.Property;
import org.openepics.discs.conf.ent.PropertyValueUniqueness;
import org.openepics.discs.conf.ent.Unit;
import org.openepics.discs.conf.mail.MailService;
import org.openepics.discs.conf.security.SSOSessionService;
import org.openepics.discs.conf.security.SecurityPolicy;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

/**
 * Implementation of data loader for properties.
 *
 * @author <a href="mailto:andraz.pozar@cosylab.com">Andraž Požar</a>
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */
@Stateless
@PropertiesLoader
public class PropertiesDataLoader extends AbstractDataLoader implements DataLoader {
    private static final Logger LOGGER = Logger.getLogger(PropertiesDataLoader.class.getCanonicalName());

    // Header column name constants
    protected static final String HDR_NAME = "NAME";
    protected static final String HDR_UNIT = "UNIT";
    protected static final String HDR_DATATYPE = "DATA-TYPE";
    protected static final String HDR_DESC = "DESCRIPTION";
    protected static final String HDR_UNIQUE = "UNIQUE";
    protected static final String HDR_REGEXP = "REGULAR-EXPRESSION";

    private static final int COL_INDEX_NAME = 1;
    private static final int COL_INDEX_DESC = 2;
    private static final int COL_INDEX_DATATYPE = 3;
    private static final int COL_INDEX_UNIT = 4;
    private static final int COL_INDEX_UNIQUE = 5;
    private static final int COL_INDEX_REGEXP = 6;

    @Inject
    private PropertyEJB propertyEJB;
    @Inject
    private DataTypeEJB dataTypeEJB;
    @Inject
    private UnitEJB unitEJB;
    @Inject
    private SecurityPolicy securityPolicy;
    @Inject
    private SSOSessionService sessionService;
    @Inject
    private MailService mailService;

    /**
     * Cached properties
     */
    private Map<String, Property> propertyByName;

    // Row data for individual cells within a row
    private String nameFld, unitFld, dataTypeFld, descFld, regexFld;
    private PropertyValueUniqueness uniqueFld;

    /**
     * Local cache of all properties by their names to speed up operations.
     */
    @Override
    protected void init() {
        super.init();

        propertyByName = new HashMap<>();
        for (Property property : propertyEJB.findAll()) {
            propertyByName.put(property.getName(), property);
        }
    }

    @Override
    protected @Nullable Integer getUniqueColumnIndex() {
        return COL_INDEX_NAME;
    }

    @Override
    protected void assignMembersForCurrentRow() {
        nameFld = readCurrentRowCellForHeader(COL_INDEX_NAME);
        unitFld = readCurrentRowCellForHeader(COL_INDEX_UNIT);
        dataTypeFld = readCurrentRowCellForHeader(COL_INDEX_DATATYPE);
        descFld = readCurrentRowCellForHeader(COL_INDEX_DESC);
        uniqueFld = uniquenessAsValue(readCurrentRowCellForHeader(COL_INDEX_UNIQUE));
        regexFld = readCurrentRowCellForHeader(COL_INDEX_REGEXP);
    }

    @Override
    protected void handleUpdate(String actualCommand) {
        checkRequired();
        if (result.isRowError()) return;

        if (propertyByName.containsKey(nameFld)) {
            try {
                final Property propertyToUpdate = propertyByName.get(nameFld);
                propertyToUpdate.setDescription(descFld);
                final boolean inUse = propertyEJB.isPropertyUsed(propertyToUpdate);
                setPropertyUnit(propertyToUpdate, unitFld, inUse);
                setPropertyDataType(propertyToUpdate, dataTypeFld, inUse);
                setPropertyUniqueness(propertyToUpdate, uniqueFld, inUse);
                setPropertyRegex(propertyToUpdate, regexFld, inUse);
                if (!result.isRowError()) {
                    propertyEJB.save(propertyToUpdate);
                }
            } catch (EJBTransactionRolledbackException e) {
                handleLoadingError(LOGGER, e);
            }
        } else {
            result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_NAME, nameFld);
        }
    }

    @Override
    protected void handleCreate(String actualCommand) {
        checkRequired();
        if (result.isRowError()) return;

        if (!propertyByName.containsKey(nameFld)) {
            try {
                final Property propertyToAdd = new Property(nameFld, descFld);
                setPropertyUnit(propertyToAdd, unitFld, false);
                setPropertyDataType(propertyToAdd, dataTypeFld, false);
                setPropertyUniqueness(propertyToAdd, uniqueFld, false);
                setPropertyRegex(propertyToAdd, regexFld, false);
                if (!result.isRowError()) {
                    propertyEJB.add(propertyToAdd);
                    propertyByName.put(propertyToAdd.getName(), propertyToAdd);
                }
            } catch (EJBTransactionRolledbackException e) {
                handleLoadingError(LOGGER, e);
            }
        } else {
            result.addRowMessage(ErrorMessage.NAME_ALREADY_EXISTS, HDR_NAME, nameFld);
        }
    }


    @Override
    protected void handleDelete(String actualCommand) {
        try {
            final Property propertyToDelete = propertyByName.get(nameFld);
            if (propertyToDelete == null) {
                result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_NAME, nameFld);
            } else {
                propertyEJB.delete(propertyToDelete);
                propertyByName.remove(propertyToDelete.getName());
            }
        } catch (EJBTransactionRolledbackException e) {
            handleLoadingError(LOGGER, e);
        }
    }

    private void setPropertyUnit(Property property, @Nullable String unit, final boolean inUse) {
        if (unit != null) {
            final Unit newUnit = unitEJB.findByName(unit);
            if (newUnit != null) {
                // is modification allowed
                if (inUse && !newUnit.equals(property.getUnit())) {
                    result.addRowMessage(ErrorMessage.MODIFY_IN_USE, HDR_UNIT, unit);
                } else {
                    property.setUnit(newUnit);
                }
            } else {
                result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_UNIT, unit);
            }
        } else {
            if (inUse && (property.getUnit() != null)) {
                result.addRowMessage(ErrorMessage.MODIFY_IN_USE, HDR_UNIT);
            } else {
                property.setUnit(null);
            }
        }
    }

    private void setPropertyDataType(Property property, String dataType, final boolean inUse) {
        final DataType newDataType = dataTypeEJB.findByName(dataType);
        if (newDataType != null) {
            if (inUse && !newDataType.equals(property.getDataType())) {
                result.addRowMessage(ErrorMessage.MODIFY_IN_USE, HDR_DATATYPE, dataType);
            } else {
                property.setDataType(newDataType);
            }
        } else {
            result.addRowMessage(ErrorMessage.ENTITY_NOT_FOUND, HDR_DATATYPE, dataType);
        }
    }

    private void setPropertyUniqueness(Property property, PropertyValueUniqueness unique, final boolean inUse) {
        if (inUse && property.getValueUniqueness() != unique) {
            result.addRowMessage(ErrorMessage.MODIFY_IN_USE, HDR_UNIQUE, unique.toString());
        } else {
            property.setValueUniqueness(unique);
        }
    }

    private PropertyValueUniqueness uniquenessAsValue(String uniqueness) {
        PropertyValueUniqueness uniquenessValue = PropertyValueUniqueness.NONE;
        if (uniqueness != null) {
            try {
                uniquenessValue = PropertyValueUniqueness.valueOf(uniqueness.trim().toUpperCase());
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINE, "Incorrect value for property uniqueness.", e);
                result.addRowMessage(ErrorMessage.UNIQUE_INCORRECT, HDR_UNIQUE, uniqueness);
            }
        }
        return uniquenessValue;
    }

    private void setPropertyRegex(Property property, @Nullable String regexp, final boolean inUse) {
        if (regexp != null) {
            try {
                Pattern.compile(regexp);
            } catch (PatternSyntaxException e) {
                LOGGER.log(Level.FINE, "The regular expression syntax is invalid.", e);
                result.addRowMessage(ErrorMessage.REGEXP_INCORRECT, HDR_REGEXP, regexp);
            }
            property.setRegexp(regexp);
        }
    }

    @Override
    public int getDataWidth() {
        return 7;
    }

    @Override
    protected void setUpIndexesForFields() {
        final Builder<String, Integer> mapBuilder = ImmutableMap.builder();

        mapBuilder.put(HDR_NAME, COL_INDEX_NAME);
        mapBuilder.put(HDR_DESC, COL_INDEX_DESC);
        mapBuilder.put(HDR_UNIT, COL_INDEX_UNIT);
        mapBuilder.put(HDR_DATATYPE, COL_INDEX_DATATYPE);
        mapBuilder.put(HDR_UNIQUE, COL_INDEX_UNIQUE);
        mapBuilder.put(HDR_REGEXP, COL_INDEX_REGEXP);

        indicies = mapBuilder.build();
    }

    private void checkRequired() {
        if (Strings.isNullOrEmpty(descFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_DESC);
        }
        if (Strings.isNullOrEmpty(dataTypeFld)) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_DATATYPE);
        }
        if (Strings.isNullOrEmpty(readCurrentRowCellForHeader(COL_INDEX_UNIQUE))) {
            result.addRowMessage(ErrorMessage.REQUIRED_FIELD_MISSING, HDR_UNIQUE);
        }
    }

    @Override
    public DataLoaderResult loadDataToDatabase(List<Pair<Integer, List<String>>> inputRows,
            Map<String, Object> contextualData, ByteArrayInputStream inputStream) {
        DataLoaderResult result = super.loadDataToDatabase(inputRows, contextualData, inputStream);
        // Mail notification
        if (!result.isError()) {
	        inputStream.reset();
	        mailService.sendMail(securityPolicy.getAllAdministratorUsernames(), sessionService.getLoggedInName(),
	                "Properties imported notification",
	                "Properties have been imported in the CCDB" + " (by "
	                        + securityPolicy.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")",
	                Arrays.asList(inputStream), Arrays.asList("imported_properties_data.xlsx"), true, true);
        }
        return result;
    }
}
