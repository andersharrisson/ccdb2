pipeline {

    agent {
        docker {
            image 'europeanspallationsource/oracle-jdk-maven-jenkins:8'
            label 'docker'
        }
    }

    environment {
        GIT_TAG = sh(returnStdout: true, script: 'git describe --exact-match || true').trim()
    }

    stages {
        stage('Build') {
            steps {
                sh 'mvn -Dmaven.test.failure.ignore clean install'
            }
        }
        stage('SonarQube analysis') {
            steps {
                withCredentials([string(credentialsId: 'sonarqube', variable: 'TOKEN')]) {
                    sh 'mvn -Dsonar.scm.disabled=true -Dsonar.login=${TOKEN} sonar:sonar'
                }
            }
        }
        stage('Publish') {
            when {
                not { environment name: 'GIT_TAG', value: '' }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'artifactory', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'mvn -Dartifactory.username=${USERNAME} -Dartifactory.password=${PASSWORD} deploy'
                }
            }
        }
    }

    post {
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }
}
